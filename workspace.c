#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define MAX_DATA 512
#define MAX_ROWS 100

/*Set of structs*/
struct Address {            //Struct store infor of Opject
    int id;
    int set;
    char name[MAX_DATA];
    char email[MAX_DATA];
};

struct Database {           // Set of Object
    struct Address rows[MAX_ROWS];
};

struct Connection {         //Struct store file content and link to Address
    FILE *file;
    struct Database *db;
};

/*Set of functions*/
void die(const char *message);  //Check error
/*Function of class Address*/
void Address_print(struct Address *addr);   //Print address of object 
/*Function of class Connection*/
struct Connection *Database_open(const char *filename, char mode);   //Connect to database
/*Function  of class Database*/
void Database_close(struct Connection *conn);   //Close database, free memory
void Database_write(struct Connection *conn);   //Write to database, call when create or set database 
void Database_create(struct Connection *conn);  //Create database
void Database_set(struct Connection *conn, int id, const char *name, const char *email);    //Set Database
void Database_get(struct Connection *conn, int id);
void Database_delete(struct Connection *conn, int id);
void Database_list(struct Connection *conn);
void Database_load(struct Connection *conn);

int main(int argc, char *argv[]){
    if(argc < 3) die("USAGE: ex17 <dbfile> <action> [action params]");
    char *filename = argv[1];
    char action = argv[2][0];
    struct Connection *conn = Database_open(filename, action);
    int id = 0;
    if(argc > 3) id = atoi(argv[3]);
    if(id >= MAX_ROWS) die("There's not that many records.");
    switch(action) {
        case 'c':                   
            Database_create(conn);
            Database_write(conn);
            break;
        case 'g':
            if(argc != 4) die("Need an id to get");
            Database_get(conn, id);
            break;
        case 's':
            if(argc != 6) die("Need id, name, email to set");
            Database_set(conn, id, argv[4], argv[5]);
            Database_write(conn);
            break;
        case 'd':
            if(argc != 4) die("Need id to delete");Database_delete(conn, id);
            Database_write(conn);
            break;
        case 'l':
            Database_list(conn);
            break;
        default:
            die("Invalid action, only: c=create, g=get, s=set, d=del, l=list");
    }
    Database_close(conn);
    return 0;
}

//Check error when run 
void die(const char *message){
    if(errno) {
        perror(message);            //Cho nay chua hieu tai sao
    } else {
        printf("ERROR: %s\n", message);
    }
exit(1);
}

//Address print
void Address_print(struct Address *addr){
    printf("%d %s %s\n", addr->id, addr->name, addr->email);
}

//Open database 
struct Connection *Database_open(const char *filename, char mode){
    //Memory allocation to connection
    struct Connection *conn = malloc(sizeof(struct Connection));
    if(!conn) die("Memory error, can't open connection to database");

    //Memory allocation to Database
    conn -> db = malloc(sizeof(struct Database));
    if(!conn->db) die("Can't allocate memory to database");

    //Read the file to memory
    if (mode == 'c'){
        conn->file = fopen(filename,"w")        //Read only
    }else{
        conn->file = fopen(filename, "r+");     //Read and write
        if (conn->file){
            Database_load(conn);
        }
    }

    //if read fail, print the error
    if (!conn->file){
        die("Read file fail");
    }
    return conn;
}

//To close file and free memory in the end of program
void Database_close(struct Connection *conn){
    //Check
    if(conn){
        //Close the file
        if(conn->file) fclose(conn->file);
        if(conn->db) free(db);
        free(conn);
    }
}
//Function to load a file to database
void Database_load(struct Connection *conn){
    int rc = fread(conn->db, sizeof(struct Database), 1, conn->file);
    if(rc!=1)   die("Fail to load the file");
}

//Function to write from database to file
void Database_write(struct Connection *conn){
    rewind(conn->file);         //Move the ptr to the begin of file
    int rc = fread(conn->db, sizeof(struct Database), 1, conn->file);
    if(rc!=1) die("Fail to Write the file");
}

//Function to create database. to give the memory to store the file
void Database_create(struct Connection *conn){
    int i=0;

    for ( i = 0; i < MAX_ROWS; i++){
        // make a prototype to initialize it
        struct Address addr = {.id = 0, .set = 0};
        //assign
        conn->db->rows[i] = addr;
    }
}

//
void Database_set(struct Connection *conn, int id, const char *name, const char *email){
    struct Address *addr = &conn->db->rows[id];
    if(addr->set) die("Already set, delete it first");
    addr->set = 1;
    // WARNING: bug, read the "How To Break It" and fix this
    char *res = strncpy(addr->name, name, MAX_DATA);
    // demonstrate the strncpy bug
    if(!res) die("Name copy failed");if(!res) die("Name copy failed");
    res = strncpy(addr->email, email, MAX_DATA);
    if(!res) die("Email copy failed");
}

void Database_get(struct Connection *conn, int id){
    struct Address *addr = &conn->db->rows[id];
    if(addr->set) {
        Address_print(addr);
    } else {
        die("ID is not set");
    }
}

//Function to del an object
void Database_delete(struct Connection *conn, int id){
    struct Address addr = {.id = id, .set = 0};
    conn->db->rows[id] = addr;
}
//Function to list object had been added
void Database_list(struct Connection *conn){
    int i = 0;
    struct Database *db = conn->db;
    for(i = 0; i < MAX_ROWS; i++) {
        struct Address *cur = &db->rows[i];
        if(cur->set) {
            Address_print(cur);
        }
}
}