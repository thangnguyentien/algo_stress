#include<stdio.h>
#include<stdlib.h>

//tao node
struct linkedList
{
    int data;
    struct linkedList *link;
};

/*Tạo ra linked List và gán giá trị*/
int main(){
    struct linkedList *head = NULL;         //Tao ra con tro 
    head = (struct linkedList *)malloc(sizeof(struct linkedList));
    head->data = 45;
    head->link = NULL;

    struct linkedList *current = malloc(sizeof(struct linkedList));
    current->data = 98;
    current->link = NULL;
    head->link = current;
}
/*Phương pháp này có một nhược điểm là sau khi chuyển qua node khác, không có các nào để truy cập node trước
  Để khắc phục nhược điểm đó, ta tạo ra con trỏ cho mỗi node, được thể hiện ở phương pháp 2*/


/*Phương pháp 3*/
int main(){
    struct linkedList *head = NULL;         //Tao ra con tro 
    head = (struct linkedList *)malloc(sizeof(struct linkedList));
    head->data = 45;
    head->link = NULL;

    struct linkedList *current = malloc(sizeof(struct linkedList));
    current->data = 98;
    current->link = NULL;
    head->link = current;

    current = malloc(sizeof(struct linkedList));
    current->data = 3;
    current->link = NULL;

    head->link->link = current;
    return;
}

/*Bài toán đếm số lượng note trong list*/
int main(){
    count_of_nodes(head);
}

void count_of_nodes(struct linkedList *head){
    int count = 0;
    if (head == NULL)
    {
        printf("Linked list is empty");
    }
    struct node *ptr = NULL;
    ptr = head;
    while (ptr != NULL)
    {
        count++;
        ptr = ptr->link;
    }
    printf("%d", count);
}


/*Bài tập đếm số node của linked list*/
void count_of_nodes(struct linkedList *head){
    int count = 0;
    if (head == NULL)
    {
        printf("Linked list is empty");
    }
    struct linkedList *ptr = NULL;
    ptr = head;
    while (ptr != NULL)
    {
        count++;
        ptr = ptr->link;
    }
    printf("%d", count);
}

int main(){
	struct linkedList *head = NULL;
    count_of_nodes(head);
}


/*Chèn node vào cuối danh sách*/

struct node
{
    int data;
    struct node *link;
};

int main(){
    struct node *head = malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = head;    //con trỏ ptr lúc này cũng trỏ chung địa chỉ với head
    ptr = add_at_end(ptr, 98);  //Tạo thêm một node ở phía sau và gán ptr chỉ vào đó
    ptr = add_at_end(ptr, 3);   //Tạo thêm một node ở phía sau và gán ptr chỉ vào đó
    ptr = add_at_end(ptr, 57);  

    /*In các phần tử của list*/
    ptr = head;         //Đưa con trỏ ptr trở về vị trí ban đầu

    while (ptr != NULL)     //loop để in các phần tử
    {
        printf("%d", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}

/*Hàm cho phép gán node vào cuối*/
struct node *add_at_end(struct node *head, int data){
    struct node *temp, *ptr;
    ptr = head;         //con trỏ ptr trỏ vào đầu danh sách
    temp = (struct node*)malloc(sizeof(struct node));       //Cấp phát bộ nhớ cho biến temp để chứa node muốn thêm
                                                            //Hiện giờ pointer đang trỏ vào phần tử muốn thêm

    temp->data = data;          //Gán giá trị
    temp->link = NULL;          //Gán link của node cuối cùng là NULL

    while (ptr->link != NULL)       //Nếu con trỏ ptr đang không trỏ vào phần tử cuối, gán để trỏ tới phần tử tiếp theo
    {   
        ptr = ptr->link;            //Gán con trỏ tới phẩn tử tiếp theo
    }
    ptr->link = temp;           //Gán node vào cuối  
    return temp;
}

/*Hàm cho phép gán phần tử vào đầu*/

int main(){
    struct node *head = malloc(sizeof(struct node));    //Khởi tạo node đầu tiên
    head->data = 45;
    head->link = NULL;

    struct node *ptr = malloc(sizeof(struct node));     //Khởi tạo node thứ 2 cũng là
    ptr->data = 98;                                     // node cuối cùng
    ptr->link  = NULL;

    head->link = ptr;                                   //Gán 2 node lại để tạo nên linked-list

    int data = 3;               //giá trị này sẽ được gán vào hàm add_beg
    head = add_beg(head, data);
    ptr = head;
}

struct node *add_beg(struct node *head, int data){
    struct node *ptr;
    
    /*Tạo node mới và khởi tạo giá trị*/
    ptr = (struct node*)malloc(sizeof(struct node));       //Cấp phát bộ nhớ cho biến temp để chứa node muốn thêm
                                                            //Hiện giờ pointer đang trỏ vào phần tử muốn thêm
    ptr->data = data;          //Gán giá trị
    ptr->link = NULL;          //

    ptr->link = head;           //Node mới dẫn tới node head cũ
    head = ptr;                 //Node head mới trở thành node vừa thêm
    
    return head;                //trả về con trỏ dẫn đến node đầu mới 
}

/*Hàm cho phép gán phần tử vào bất kỳ vị trí nào*/

/*Các bước thực hiện
    1. Tạo ra một node mới
    2. Đưa con trỏ thao tác tới vị trí cần thêm
    3. Update the link*/

/*Chương trình mẫu*/
int main(){
    struct node *head = malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    add_at_end(head, 98);
    add_at_end(head, 23);

    add_at_pos(head, data, position);
    struct node *ptr = head;

    while (ptr != NULL)
    {
        printf("%d", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}

void add_at_pos(struct node*head, int data, int pos){
    struct node *ptr = head;            //Biến con trỏ trỏ vào node đầu
    
    struct node *ptr2 = malloc(sizeof(struct node));        //Khai báo node thứ 2
    ptr2->data = data;          //Gán cho node thứ 2
    ptr2->link = NULL;

    pos--;              //calib biến đếm
    while (pos != 1)    //Cho con trỏ chạy tới vị trí cần chèn
    {
        ptr = ptr->link;
    }

    ptr2->link = ptr->link;     //gán biến link của node cần thêm
    ptr->link  = ptr2;          //Gán biến link của node liền trước vào node mới thêm
}

