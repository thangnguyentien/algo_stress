#include<stdio.h>


/*Ví dụ sau mô tả việc sử dụng một con trỏ hàm*/

//function Prototype
int subtract(int a, int b);

int main(){
    int (*subtractINT)(int a, int b);
    subtractINT = subtract;

    int y = (*subtractINT)(10, 2);
    printf("Subtract gives %d", y);

    int z = subtractINT(10, 2);
    printf("Subtract gives %d", z);
}

int subtract(int a, int b){
    return a-b;
}

/*Ví dụ sau mô tả cách đưa con trỏ hàm vào một hàm khác như một tham số*/

//Function Prototype
int add(int x, int y);
int subtract(int x, int y);
int domath(int (*mathop), int x, int y);

//add
int add(int x, int y){
    return x + y;
}

//subtract
int  subtract(int x, int y){
    return x - y;
}

//run the function pointer as input
int domath(int (*mathop)(int, int), int x, int y){
    return (*mathop)(x, y);
}

//calling from main
int main(){
    //call math function with add
    int a = domath(add, 10, 2);
    printf("Add gives: %d\n", a);
    //call math function with subtract
    int a = domath(subtract, 10, 2);
    printf("Add gives: %d\n", a);
}

/*Ví dụ sau mô tả con trỏ hàm như là một kết quả của bảng tra*/
void do_start_task(){
    printf("Start task");
}

void do_stop_task(){
    printf("Stop task");
}

void do_up_task(){
    printf("Up task");
}

void do_down_task(){
    printf("Down task");
}

void do_left_task(){
    printf("Left task");
}

void do_right_task(){
    printf("right task");
}

int main(){
    int func_number;
    void (*fp) (void);  /*fp is  a pointer to a function*/

    while (1)
    {
        printf("\n Select a function 1-6");
        scanf("%d", &func_number);
        if ((func_number > 0) && (func_number < 7))
        {
            fp = task_list[func_number - 1];
            (*fp)();
        }
        
    }

void (*task_list[6])(void) = {
    do_down_task,
    do_left_task,
    do_right_task,
    do_start_task,
    do_stop_task,
    do_up_task
};
    
}