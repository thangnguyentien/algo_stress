#define LED_SET_DIRECTION (P1DIR)
#define LED_REGISTER (P1OUT)
#define LED_BIT (1<<3)

LED_SET_DIRECTION |= LED_BIT; // set the IO to be output
LED_REGISTER |= LED_BIT; // turn the LED on
LED_REGISTER &= ~LED_BIT; // turn the LED off