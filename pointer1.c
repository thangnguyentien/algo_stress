#include <stdio.h>

int main(int argc, char *argv[]){

    //Khai bao cac gia tri ten va tuoi
    int age[] = {12, 43,34,69,34,12};
    char *name[] = {"Thang", "Nguyen", "Tien", "Hong", "Anh"};

    //Tinh size cua mang
    int size = sizeof(age)/sizeof(int);
    int i = 0; //Bien dem

    //Khai bao cac con tro
    char **cur_name = name;
    int *cur_age    =   age;

    //Dem bang phan tu mang
    for ( i = 0; i < size; i++){
        printf("ban %s co so tuoi la %d\n", name[i], age[i]);
    }

    //Dem bang con tro
        for ( i = 0; i < size; i++){
        printf("ban %s co so tuoi la %d\n", *(name+i), *(age+i));
    }
    
return 0;
}