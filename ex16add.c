/*Chuong trinh su dung struct va con tro*/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

/*the same with the base file*/

/*Function Person_Create : To Create Structre's instances
    - Use malloc for "mermory allocate", ask to a piece of raw memory
    - Get the size of struct by malloc and sizeof
    - Use assert to make sure having a valid piece of memory . There's a special constant called NULL that you use to mean 
      "unset or invalid pointer". This assert is basically checking that malloc didn't return a NULL invalid pointer
    - Initiate east field of Struct using x -> y
    - Function strdup to make sure that the structs owns the string
    - Function Person_destroy to free memory
    - Function Persn_print to print the concept */

/*Main Function
    - Create two people, joe & Clare
    - Print them out, using %p format
    - Both 20 yrs old
    - Finally destroy*/

/*Khai bao stuct Person co cac elements la name, age, height, weight*/

struct Person{
    /* data */
    char *name;
    int age;
    int height;
    int weight;
};

struct Person *Person_create(char *name, int age, int height, int weight)
{
    struct Person *who = malloc(sizeof(struct Person));
    assert(who != NULL);

    who->name = strdup(name);
    who->age = age;
    who->height = height;
    who->weight = weight;

    return who;
};

void Person_destroy(struct Person *who){
    assert(who != NULL);

    free(who->name);
    free(who);
}

void Person_print(struct Person *who){
    printf("Name: %s\n", who->name);
    printf("Name: %d\n", who->age);
    printf("Name: %d\n", who->height);
    printf("Name: %d\n", who->weight);
}

int main(int argc, char *argv[]){

    //make two people  structures
    struct Person *joe = Person_create("joe Nguyen", 34, 176, 76);
    struct Person *Clare = Person_create("Clare Vu", 26, 165,49);

    //Print them out when they're in the memory
    printf("Joe is at memory location %p:\n", joe);
    Person_print(joe);
    printf("Clare is at memory location %p:\n", Clare);
    Person_print(Clare);

    // make everyone age 20 years and print them again
    joe->age += 20;
    joe->height -= 2;
    joe->weight += 40;
    Person_print(joe);
    Clare->age += 20;
    Clare->weight += 20;
    Person_print(Clare);
    // destroy them both so we clean up
    Person_destroy(joe);
    Person_destroy(Clare);
    return 0;
}

/*Truyen NULL vao ham Print se dan toi loi ko cap phat bo nho*/

    
