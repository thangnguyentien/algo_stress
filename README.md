This resource were built by Iosif Vissarionovich Stalin

        ***     Nothing happier than pursuing ideals    ***
***  Nothing more glorious than sacrificing for the motherland ***

###License : Free   

Introduction

Know the theoretical knowledge of the algorithm is essential, but it is not sufficient. You need to have a
systematic approach to solve a problem. Our approach is fundamentally different to solve any algorithm
design question. We will follow a systematic five-step approach to reach to our solution. Master this
approach and you will be better than most of the candidates in interviews.
Five steps for solving algorithm design questions are:
1. Constraints
2. Ideas Generation
3. Complexities
4. Coding
5. Testing


I. Constraints

Solving a technical question is not just about knowing the algorithms and designing a good software
system. The interviewer wants to know you approach towards any given problem. Many people make
mistakes as they do not ask clarifying questions about a given problem. They assume many things and
begin working with that. There is data, which is missing that you need to collect from your interviewer
before beginning to solve a problem.

In this step, you will capture all the constraints about the problem. We should never try to solve a problem
that is not completely defined. Interview questions are not like exam paper where all the details about a
problem are well defined. In the interview, the interviewer actually expects you to ask questions and
clarify the problem.

For example : When the problem statement says that write an algorithm to sort numbers.
The first information you need to capture is what king of data. Let us suppose interviewer respond with
the answer Integer.
The second information that you need to know what is the size of data. Your algorithm differs if the input
data size if 100 integers or 1 billion integers.
Basic guideline for the Constraints for an array of numbers:
1. How many numbers of elements in the array?
2. What is the range of value in each element? What is the min and max value?
3. What is the kind of data in each element is it an integer or a floating point?
4. Does the array contain unique data or not?
Basic guideline for the Constraints for an array of string:
1. How many numbers of elements in the array?
2. What is the length of each string? What is the min and max length?
3. Does the array contain unique data or not?
Basic guideline for the Constraints for a Graph
1. How many nodes are there in the graph?
2. How many edges are there in the graph?
3. Is it a weighted graph? What is the range of weights?
4. Is the graph directed or undirected?
5. Is there is a loop in the graph?
6. Is there negative sum loop in the graph?
7. Does the graph have self-loops?
We have already seen this in graph chapter that depending upon the constraints the algorithm applied
changes and so is the complexity of the solution.

II. Idea Generation

We have covered a lot of theoretical knowledge in this book. It is impossible to cover all the questions as
new ones are created every day. Therefore, we should know how to handle new problems. Even if you
know the solution of a problem asked by the interviewer then also you need to have a discussion with the
interviewer and reach to the solution. You need to analyse the problem also because the interviewer may
modify a question a little bit and the approach to solve it will vary.
Well, how to solve a new problem? The solution to this problem is that you need to do a lot of practice
and the more you practice the more you will be able to solve any new question, which come in front of
you. When you have solved enough problems, you will be able to see a pattern in the questions and able
to solve new problems easily.
Following is the strategy that you need to follow to solve an unknown problem:
1. Try to simplify the task in hand.
2. Try a few examples
3. Think of a suitable data-structure.
4. Think about similar problems you have already solved.

III. Complexities