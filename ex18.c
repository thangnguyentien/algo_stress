#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

/** Our old friend die from ex17. */
void die(const char *message){
    if (errno)
    {
        perror(message);
    }else
    {
        printf("ERROR: %s\n", message);
    }
    
    exit(1);
}

/*Compare by subtract two number*/
int sorted_order(int a, int b){
    return a-b;
}
/*Compare by reverse subtract*/
int reverse_order(int a, int b){
    return b-a;
}
/*Compare by a mod b*/
int strange_order(int a, int b){
    if (a == 0 || b == 0)
    {
        return 0;               //Return 0 if the two number = 0
    }else {
        return a % b;           //Return a%b
    }
    
}

typedef int (*compare_cb)(int a, int b);

void test_sort(int *number, int count,compare_cb cmb){
    int i;
    int *sort = bubble_sort(numbers, count, cmp);
    if(!sorted) die("Failed to sort as requested.");

    for (i = 0; i < count; i++){
        printf("%d ", sort[i];)
    }
    printf("/n");
    free(sort);
}

int* bubble_sort(int *number, int count, compare_cb){
    int i,j;
    int temp;
    int *target = malloc(count*sizeof(int));
    for (i = 0; i < count; i++)
    {
        for ( j = 0; i < count; j++)
        {
            if(cmp(target[j], target[j+1]) > 0) {
                temp = target[j+1];
                target[j+1] = target[j];
                target[j] = temp;
            }
        }
        
    }
    return target;
    
}


int main(int argc, char *argv[]){
    /*Check input valid*/
    if (argc < 2) die("USAGE: ex18 4 3 1 5 6");

    /*Create index to numeric*/
    int count = argc - 1;       //Because input string include the name of file, so we must subtract 1
    int i = 0;
    char **input = argv + 1;    //Move pointer to the start of data string

    int *number = malloc(count * sizeof(int));  //allocate memory
    if(!number)  die("Fail to store data");

    for(i = 0; i < count; i++){
        number[i] = atoi(input[i]);
    }
    
    
}

