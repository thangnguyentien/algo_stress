/*Searching Algorithms*/

/*  · Linear Search – Unsorted Input
    · Linear Search – Sorted Input
    · Binary Search (Sorted Input)
    · String Search: Tries, Suffix Trees, Ternary Search.
    · Hashing and Symbol Tables*/


/*  01. Linear Search – Unsorted Input  */

int linearSearchUnsorted(int arr[], int size, int value)
{
int i = 0;
for(i = 0 ; i < size ; i++)
    {   if(value == arr[i] )
        return i;
    }
    return -1;
}

/*  02. Linear Search – Sorted Input  */

int linearSearchSorted(int arr[], int size, int value)
{
int i = 0;
for(i = 0 ; i < size ; i++)
    {if(value == arr[i] )
        return i;
    else if( value < arr[i] )
        return -1;
    }   
    return -1;
}

/*  03. Binary Search  
    Note: Binary search requires the array to be sorted
     otherwise binary search cannot be applied.*/

int binarySearch(int arr[], int size, int value){
    int low = 0;
    int high = size - 1;
    int mid;

    while(low <= high){
        mid = low + (high = low)/2 ;
        if(arr[mid] == value){
            return mid;
        }
        else if(arr[mid] < value){
            low = mid + 1;
        }
        else {
            high = mid - 1; 
        }
    }
    return -1;
}

/*String Searching Algorithms
Refer String chapter*/

#include <stdio.h>

int main(){
    int a,b;
    scanf("%d %d",&a,&b);
    printf("%d",a+b);
    return 0;
}