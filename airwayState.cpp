/*Chuyen doi Code tu tuyen tinh sang state machine*/
#include<stdio.h>
#include<conio.h>
#include<string.h>

typedef enum tagState{
    IDLE,
    ECO,
    BUS,
    CHANGE,
    PRINT,
    END
}state;

typedef struct passenger{	// Struct luu thong tin nguoi dung
	char *name;
	char *id;
	int seat;
}passenger;

/*Global Variable Set*/
int arrSeat[10]= {0};		//Bien luu cac seat
passenger arrPass[10];			//Mang luu nguoi dung

/*Function set*/
int choice(state *state);
int checkEco(int *arrSeat, state *state);
int checkBs(int *arrSeat, state *state);
int swClass();
void getInfor(passenger *arrPass, int *arrSeat);

/*GLobal Var Set*/
state stateV = IDLE;
state *stateA = &stateV;

int main(){

    for(int i = 0; i < 10; i++)
    {
        switch (stateV)
        {
        case IDLE:
            /* Function : Get user's choice */
            // printf("Case is IDLE");
            choice(stateA);
            break;
        case ECO:
            /* Check ECO seat */
            // printf("Case is ECO");
            checkEco(arrSeat, stateA);
            break;
        case BUS:
            /* Check BUS seat */
            checkBs(arrSeat, stateA);
            break;
        case CHANGE:
        /* Ask user to change the seat */
            if(swClass() == 2){
                stateV = IDLE;
            }else{
                stateV = END;
            }
            break;
        case PRINT:
        /* Collect user infors & print */
            getInfor(arrPass, arrSeat);
            stateV = END;
            break;                  
        case END:
        /* code */
            printf("Your transaction is completed\n");
            stateV =  IDLE;
            break;
        default:
            break;
        }
    }
    
}

int choice(state *state){
    int choice;
    printf("Please choose your class: 1 for Eco & 2 for bussiness: ");
    scanf("%d", &choice);
    if(choice == 1){
        *state = ECO;
    }else *state = BUS;
    return choice;
}

int checkEco(int *arrSeat, state *state){
    for(int i = 5; i < 9; i++){
		if(arrSeat[i] != 1){
			arrSeat[i] = 1;
            *state = PRINT;
			return 1;
		}else{
            *state = CHANGE;
            return 0;
        } 
	}
}

int checkBs(int *arrSeat, state *state){
    for(int i = 0; i < 4; i++){
		if(arrSeat[i] != 1){
			arrSeat[i] = 1;
            *state = PRINT;
			return 1;
		}else{
            *state = CHANGE;
            return 0;
        } 
	}
}

int swClass(){
    int switchS;
    printf("Do you want to switch the class: press 1 for no, 2 for yes : ");
	scanf("%d", &switchS);
    return switchS;
}

void getInfor(passenger *arrPass, int *arrSeat){
	int i;
    printf("Please input your information.\n");
    char a[10];
	arrPass[i].seat = arrSeat[i];
	printf("Please insert your name: ");
	scanf("%s", a);
	arrPass[i].name = a;
	char b[10];
	printf("Please insert your ID: ");
	scanf("%s", &b);
	arrPass[i].id = b;
	printf("Please check your information: \n");
	printf("ID: %s \n",arrPass[i].id);
	printf("Name : %s\n",arrPass[i].name );
	printf("Seat : %d\n",arrPass[i].seat);
	printf("\n");
}






